<?php get_header();?>

<main class="site-main">
  <div class="container-fluid">
    <div class="row">
      <!-- side bar left -->
      <div class="col-2" id="side">
        <a class="arrow-link" href="<?php echo home_url(); ?>">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/2worrA.png" alt="" class="arrow">
          </div>
        </a>
      </div>
      <!-- main contents -->
      <div class="col-8" id="main">
        <div class="row">
          <div class="col-12">
            I'm sorry, it was not found.
          </div>
        </div>
      </div>
      <!-- side bar right-->
      <div class="col-2" id="side">

      </div>
    </div>
  </div>
</main>

<!-- #main -->

<?php get_footer();?>