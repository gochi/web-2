<?php get_header();?>
<main>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-10 col-12 main" id="">
        <div class="container">
          <div class="row">
            <div class="col-8 offset-2">
              <?php get_template_part('template-parts/breadcrumb');?>
              <div class="row contents-list">

                <?php if (have_posts()):?>
                <?php while (have_posts()):the_post();?>
                <div class="col-sm-6 col-12 post-card">
                  <a href="<?php the_permalink();?>">
                    <div class="post-frame">
                      <?php the_post_thumbnail();?>
                    </div>
                    <div class="post-caption">
                      <p><?php the_title(); ?> </p>
                    </div>
                  </a>
                </div>
                <?php endwhile; ?>
                <?php endif;?>


              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="col-sm-2 col-12 side" id="">
        <a class="arrow-link" href="<?php echo home_url('contact'); ?>">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow2.png" alt="" class="arrow">
            <!-- <div class="arrow-css"></div> -->
          </div>
        </a>
      </div>
    </div>
  </div>
</main>
<?php get_footer();?>