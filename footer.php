<footer class="container-fluid">
  <div class="row">
    <div class="copyright col-md-6 col-5">
      <span>©︎ 2020 <a href="https://dazingcat.com">hokeneko</a></span>
    </div>
    <div class="sns-btn col-md-6 col-7">
      <ul class="sns-list">
        <li>
          <a href="https://twitter.com/dazingcat"><i class="fab fa-twitter"></i><span class="footer-sns">Twitter</span></a>
        </li>
        <li>
          <a href="https://www.instagram.com/migudummy/">
            <i class="fab fa-instagram"></i><span class="footer-sns">Instagram</span></a>
        </li>
      </ul>
    </div>
  </div>
</footer>

<?php wp_footer();?>
</body>

</html>
<script>
const trigger = document.getElementById('menu-btn');

trigger.addEventListener('click', () => {
  const classList = trigger.classList;

  if ([...classList].includes('active')) {
    trigger.classList.remove('active');
    return;
  }
  trigger.classList.add('active');
});

// メニュー


const nav = document.getElementById('navArea');
const btn = document.getElementById('menu-btn');
const mask = document.getElementById('mask');
const open = 'open'; // class
// menu open close
btn.addEventListener('click', function() {
  if (nav.classList.contains(open)) {
    nav.classList.remove(open);
  } else {
    nav.classList.add(open);
  }
});
// mask close
mask.on('click', function() {
  nav.classList.remove(open);
});
</script>