<?php get_header();?>
<main>
    <div class="container-fluid">
        <div class="row home-bg">
            <div class="col-12 col-sm-10 main" id="home-main">
                <div class="site-title">
                    <p class="title-text">Portfolio</p>
                </div>
            </div>
            <div class="col-12 col-sm-2" id="side">
                <a class="arrow-link" href="<?php echo home_url('/contents'); ?>">
                    <div class="arrow-box">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow2.png" alt=""
                            class="arrow">
                    </div>
                </a>
            </div>
        </div>
    </div>
</main>
<?php get_footer();?>