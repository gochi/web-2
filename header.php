<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/web-2.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/common.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/home.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/about.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/gallery.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/single.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/contact.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Radley&display=swap" rel="stylesheet">
  <link rel="apple-touch-icon" type="image/png" href="/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/icon-192x192.png">
  <script src="https://kit.fontawesome.com/4f8c92036e.js" crossorigin="anonymous"></script>
  <title><?php bloginfo('name');?></title>
  <?php wp_head();?>
</head>

<body id="navArea">
  <div id="mask"></div>
  <?php wp_body_open();?>
  <header>
    <div class="container-fluid">
      <div class="row">
        <div class="site-logo col-10">
          <a class="" href="<?php echo home_url();?>">
            <img class="top-logo-img" alt=""
              src="<?php echo get_template_directory_uri();?>/assets/images/lineonly.png">
          </a>
        </div>
        <div class="col-2" id="navArea">
          <nav>
            <div class="inner">
              <?php
              $args = array(
              'menu'=>'global-navigation',//管理画面で作成したメニューの名前
              'menu_class'=>'', //メニューを構成するulタグのクラス名
              'container'=>false, //<ul>タグを囲んでいる<div>タグを削除
              );
              wp_nav_menu($args);
              ?>
            </div>
          </nav>
          <section class="menu-btn-section">
            <div class="btn-trigger" id="menu-btn">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </section>
        </div>
      </div>
    </div>
  </header>