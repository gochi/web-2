<?php
/**
 * Template Name: about
 * Description: 自己紹介用のテンプレート
 */
?>
<?php get_header();?>
<main>
  <div class="container-fluid">
    <div class="row">
      <div class="col-10" id="main">
        <div class="container">
          <div class="row">
            <div class="col-10 offset-2">
              <div class="row">
                <div class="col-12">
                  <h2>Profile</h2>
                  <div class="myname">
                    <h4 class="about-h4">Yumi Goto</h4>
                    <p>ごとう ゆみ</p>
                  </div>
                </div>
                <div class="col-md-6 col-12 profile-inner">

                  <div class="history">
                    <h3 class="about-h3">History</h3>
                    <div class="about-txt">
                      <p>平成26年3月</p>
                      <p>某高等学校 普通科 卒業</p>
                      <p>平成30年3月</p>
                      <p>某国立大学</p>
                      <p>教育地域科学部地域科学課程 卒業</p>
                      <p>平成30年4月</p>
                      <p>SIer企業に入社</p>
                      <p>広報担当、Webデザイン</p>
                    </div>

                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <h3 class="about-h3">Skills</h3>
                  <div class="about-txt">
                    <p>Adobe Illustrator</p>
                    <p>Adobe Photoshop</p>
                    <p>Figma</p>
                    <p>HTML5</p>
                    <p>CSS3</p>
                    <p>ライティング</p>
                    <p>イラスト</p>
                    <p>Webデザイン</p>
                    <p>グラフィックデザイン</p>
                    <p>UI/UXデザイン</p>
                  </div>

                  <h3 class="about-h3">Job Experience</h3>
                  <div class="about-txt">
                    <p>社内報執筆・編集</p>
                    <p>Webサイト（PC・モバイル）デザイン</p>
                    <p>チラシデザイン</p>
                    <p>封筒デザイン</p>
                    <p>キャラクターデザイン</p>
                    <p>ロゴ刷新プロジェクトのディレクション</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="col-sm-2 col-12 side" id="">
        <a class="arrow-link" href="<?php echo home_url('/gallery'); ?>">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow2.png" alt="" class="arrow">
          </div>
        </a>
      </div>
    </div>
  </div>
</main>
<?php get_footer();?>