<?php
/**
 * Template Name: contact
 * Description: 問い合わせページ用のテンプレート
 */
?>
<?php get_header();?>
<main class="">
  <div class="container-fluid">
    <div class="row contents-bg">
      <div class="col-sm-10 col-12 main" id="">
        <div class="container">
          <div class="col-sm-8 offset-sm-2 col-12">
            <div class="contact-info">
              <p>お問い合わせはこちらから</p>
            </div>
            <div class="contact-way">
              <p><a href="https://twitter.com/dazingcat"><i class="fab fa-twitter"></i>@dazincat</a></p>
              <p><a href="https://www.instagram.com/migudummy/">
                  <i class="fab fa-instagram"></i>migudummy</a></p>
              <p><a href="mailto:address"><i class="far fa-envelope"></i>mongoose0910@gmail.com</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-2 col-12 side" id="">
        <a class="arrow-link" href="<?php echo home_url(); ?>">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow2.png" alt="" class="arrow">
          </div>
        </a>
      </div>
    </div>
  </div>

</main>
<?php get_footer();?>