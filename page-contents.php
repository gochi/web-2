<?php
/**
 * Template Name: contents
 * Description: 目次ページ用のテンプレート
 */
?>
<?php get_header();?>
<main>
  <div class="container-fluid">
    <div class="row contents-bg">
      <div class="col-sm-10 col-12" id="main">
        <div class="container">
          <div class="row">
            <div class="col-8 offset-2">
              <div class="row contents-list">
                <div class="col-sm-3 col-6 contents-card">
                  <a href="<?php echo home_url(''); ?>">
                    <div class="contents-frame">
                      <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-home-x1.png" alt=""
                        class="contents-icon">
                    </div>
                    <div class="contents-caption">
                      <p>home</p>
                    </div>
                  </a>
                </div>
                <div class="col-sm-3 col-6">
                  <a href="<?php echo home_url('/about'); ?>">
                    <div class="contents-frame">
                      <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-who-x1.png" alt=""
                        class="contents-icon">
                    </div>
                    <div class="contents-caption">
                      <p>who?</p>
                    </div>
                  </a>
                </div>
                <div class="col-sm-3 col-6">
                  <a href="<?php echo home_url('/gallery'); ?>">
                    <div class="contents-frame">
                      <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-gallery-x1.png" alt=""
                        class="contents-icon">
                    </div>
                    <div class="contents-caption">
                      <p>gallery</p>
                    </div>
                  </a>
                </div>
                <div class="col-sm-3 col-6">
                  <a href="<?php echo home_url('/contact'); ?>">
                    <div class="contents-frame">
                      <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-contact-x1.png" alt=""
                        class="contents-icon">
                    </div>
                    <div class="contents-caption">
                      <p>contact</p>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-2 col-12" id="side">
        <a class="arrow-link" href="<?php echo home_url('/about'); ?>">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow2.png" alt="" class="arrow">
            <!-- <div class="arrow-css"></div> -->
          </div>
        </a>
      </div>
    </div>
  </div>
</main>
<?php get_footer();?>