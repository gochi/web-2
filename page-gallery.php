<?php
/**
 * Template Name: gallery
 * Description: ギャラリー用のテンプレート
 */
?>
<?php get_header();?>
<main>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-10 col-12" id="main">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-12 offset-sm-2">
              <div class="row category-list">
                <div class="col-12 category-eyecatch eyecatch-web">
                  <a href="<?php echo home_url('/category/web'); ?>">
                    <div class="cat-txt">
                      <p>Web</p>
                    </div>
                  </a>
                </div>
                <!-- <div class="col-12 category-eyecatch">
                  <a href="<?php echo home_url('/category/web'); ?>">
                    <div class="arrow-gallery">
                      <img class="" src="<?php echo get_template_directory_uri();?>/assets/images/arrow-gallery.png">
                    </div>
                  </a>
                </div> -->
                <div class="col-12 category-eyecatch eyecatch-graphic ">
                  <a href="<?php echo home_url('/category/graphic'); ?>">
                    <div class="cat-txt">
                      <p>graphic</p>
                    </div>
                  </a>
                </div>

                <div class="col-12 category-eyecatch eyecatch-illust">
                  <a href="<?php echo home_url('/category/illust'); ?>">
                    <div class="cat-txt">
                      <p>illust</p>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-2 col-12" id="side">
        <a class="arrow-link" href="<?php echo home_url('/category/web'); ?>">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow2.png" alt="" class="arrow">
            <!-- <div class="arrow-css"></div> -->
          </div>
        </a>
      </div>
    </div>
  </div>
</main>
<?php get_footer();?>