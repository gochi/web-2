<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package my-ehon
 */

get_header();
?>

<main class="site-main">
  <div class="container-fluid">
    <div class="row">
      <!-- side bar left -->
      <div class="col-sm-2 col-6 side" id="single-side-2">
        <a class="arrow-link" href="<?php
          $prev_post = get_previous_post();
          if( !empty( $prev_post ) ) {
            echo get_permalink( $prev_post->ID );
          }
          ?>
        ">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/2worrA.png" alt="" class="arrow">
          </div>
        </a>
      </div>
      <!-- main contents -->
      <div class="col-sm-8 col-12 main" id="single-main">
        <div class="row">
          <div class="col-12">
            <?php get_template_part('template-parts/breadcrumb');?>
            <?php if(have_posts()):?>
            <?php while(have_posts()):the_post(); ?>
            <article id="post-<?php the_ID(); ?>" class="article">
              <div class="row post-list">

                <div class="col-12 text-center">
                  <div class="post-title">
                    <h1><?php the_title();?></h1>
                  </div>
                  <div class="time-wrapper">
                  <time datetime="<?php the_time("Y-m-d");?>">
                    <?php the_time('d.m.Y');?>
                  </time>
                  </div>
                  <div class="post-thumbnail-area">
                    <?php 
                    if (has_post_thumbnail()){ 
                      the_post_thumbnail('full');
                    }
                    else{
                      echo '<img src="'.get_template_directory_uri().'/assets/images/light-gray-x2.png" />';
                    }
                    ?>
                  </div>
                </div>
                <div class="col-sm-8 col-12 article-txt">
                  <?php the_content();?>
                </div>
                <div class="col-sm-4 col-12 post-gallery">

                </div>
              </div>
            </article>
            <?php endwhile; ?>
            <?php endif;?>
          </div>
        </div>
      </div>
      <!-- side bar right-->
      <div class="col-sm-2 col-6 side" id="single-side-2">
        <a class="arrow-link" href="<?php
          $next_post = get_next_post();
          if( !empty( $next_post ) ) {
            echo get_permalink( $next_post->ID );
          }
          ?>
        ">
          <div class="arrow-box">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow2.png" alt="" class="arrow">
          </div>
        </a>
      </div>
    </div>
  </div>



</main>
<!-- #main -->
<?php get_footer();?>