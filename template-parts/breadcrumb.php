<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package my-ehon
 */

?>

<div class="breadcrumb">
  <div class="breadcrumb_inner">
    <?php 
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
  </div>
</div>